from selenium import webdriver


def change_language_to_russian():
    driver = webdriver.Chrome()
    driver.get("https://www.binance.com/en")

    language_button = driver.find_element_by_xpath("/html/body/div[1]/div/header/div[9]/div[1]")
    print(language_button.text)
    assert language_button.text == "English"
    language_button.click()
    russian_lang_button = driver.find_element_by_xpath(
        "/html/body/div[4]/div[2]/div/div[2]/div[2]/div/div[2]/button[23]")
    russian_lang_button.click()
    language_button = driver.find_element_by_xpath("/html/body/div[1]/div/header/div[9]/div[1]")
    print(language_button.text)
    assert language_button.text == "Русский"
    driver.close()
    driver.quit()


if __name__ == '__main__':
    change_language_to_Russian()
